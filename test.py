import os
import unittest

from app import app

class BasicTests(unittest.TestCase):
	def setUp(self):
		self.app = app.test_client()
		self.assertEqual(app.debug, False)

	def tearDown(self):
		pass

	def test_main_page(self):
		response = self.app.get("/", follow_redirects=True)
		self.assertEqual(response.status_code, 200)

	def test_main_page_with_name(self):
		response = self.app.get("/wojtek/", follow_redirects=True)
		self.assertEqual(response.status_code, 200)

	def test_redirect(self):
		response = self.app.get("/link/wp/")
		self.assertEqual(response.status_code, 301)

if __name__ == "__main__":
	unittest.main()