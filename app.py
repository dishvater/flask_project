from flask import Flask, render_template, redirect, url_for

app = Flask(__name__)

@app.route("/")
def hello():
	return render_template("index.html")

@app.route("/<string:name>/")
def hello_name(name):
	return render_template("name.html", name=name)

@app.route("/link/wp/")
def redirect_to_wp():
	return redirect('http://www.wp.pl', 301)


if __name__ == "__main__":
	app.run(debug=True)